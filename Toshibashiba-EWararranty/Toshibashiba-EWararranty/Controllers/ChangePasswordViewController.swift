//
//  ChangePasswordViewController.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/9/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet weak var oldPasswordTxf: UITextField!
    @IBOutlet weak var newPasswordTxf: UITextField!
    @IBOutlet weak var confirmPasswordTxf: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Quay lại"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isHidden = false
        
        self.navigationItem.title = "THAY ĐỔI MẬT KHẨU"
        
        
        sendBtn.setBackgroundColor(color: ColorHelper.red, forState: .normal)
        sendBtn.setBackgroundColor(color: ColorHelper.lightRed, forState: .disabled)
        
        sendBtn.isEnabled = (oldPasswordTxf.text == "" || newPasswordTxf.text == "" || confirmPasswordTxf.text == "" ) ? false:true
        oldPasswordTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        newPasswordTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        confirmPasswordTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onSend(_ sender: UIButton) {
        if let newPass = newPasswordTxf.text,
            let confirmPass = confirmPasswordTxf.text,
            let oldPass = oldPasswordTxf.text,
            newPass == confirmPass {
            AppService.changePassword(username: ObjectManager.default.username, oldPassword: oldPass, newPassword: newPass) { isSuccess, error in
                if (isSuccess) {
                    let alert = UIAlertController(title: "Thông báo", message: error?.localizedDescription ?? "thay đổi mật khẩu thành công", preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: { (alertAction) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    alert.addAction(actionOk)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }else {
                    print(error?.localizedDescription ?? "change password fail")
                    
                    let alert = UIAlertController(title: "Lỗi", message: error?.localizedDescription ?? "change password fail", preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: nil)
                    alert.addAction(actionOk)
                    self.present(alert, animated: true, completion: nil)
                }
            }
                
        }else { 
            let alert = UIAlertController(title: "Lỗi", message: "Xác nhận mật khẩu không trùng khớp", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: nil)
            alert.addAction(actionOk)
            self.present(alert, animated: true, completion: nil)
            // Submit data to server
        }
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        sendBtn.isEnabled = (oldPasswordTxf.text == "" || newPasswordTxf.text == "" || confirmPasswordTxf.text == "" ) ? false:true        
    }

}
