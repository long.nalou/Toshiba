//
//  DateListCollectionViewCell.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/9/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit

class DateListCollectionViewCell: UICollectionViewCell {
    
    static let identifier:String = "dateListCell"
    class func regiterWith(collectionView: UICollectionView){
        collectionView.register(UINib(nibName: "DateListCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: identifier)
    }
    
    @IBOutlet weak var lbNameDate: UILabel!
    @IBOutlet weak var viewSelectedDate: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.initCell()
    }
    
    func initCell() {
        self.viewSelectedDate.isHidden = true
        self.lbNameDate.textColor = UIColor.gray
    }
    
    func selectedCell() {
        self.viewSelectedDate.isHidden = false
        self.lbNameDate.textColor = UIColor.red
    }
    
}
