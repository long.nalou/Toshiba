//
//  HeaderProductView.swift
//  Tosiba DS
//
//  Created by Hong Nhung on 9/8/18.
//  Copyright © 2018 Chris Nguyen. All rights reserved.
//

import UIKit

protocol showListProductDelegate: AnyObject {
    func selectShow(indexSection: Int)
}

class HeaderProductView: UIView {

    @IBOutlet weak var lbNameDate: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    weak var delegate: showListProductDelegate?
    var index: Int?
    
    func setUpView() {
        let contentView = Bundle.main.loadNibNamed("HeaderProductView", owner: self, options: nil)?.first as! UIView
        contentView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 50)
        self.addSubview(contentView)
        
        // add action call profile merchant
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showList) )
        self.addGestureRecognizer(tap)
    }
    
    @objc func showList() {
        self.delegate?.selectShow(indexSection: self.index!)
    }
    
}
