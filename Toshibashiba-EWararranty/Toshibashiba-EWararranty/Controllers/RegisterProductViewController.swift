//
//  RegisterProductViewController.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0


class RegisterProductViewController: UIViewController {
    @IBOutlet weak var model2View: UIView!
    @IBOutlet weak var dateBuyView: UIView!
    @IBOutlet weak var telephoneNumberView: UIView!
    @IBOutlet weak var fullnameView: UIView!
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var usernameTxf: UITextField!
    @IBOutlet weak var firstnameTxf: UITextField!
    @IBOutlet weak var lastnameTxf: UITextField!
    
    @IBOutlet weak var phoneNumberTxf: UITextField!
    @IBOutlet weak var modelTxf: UITextField!
    @IBOutlet weak var model2Txf: UITextField!
    @IBOutlet weak var serialNumberTxf: UITextField!
    @IBOutlet weak var dateBuyTxf: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    
    var list_Product = [ProductModel]()
    
    var isIncludedModel2View = false {
        didSet{
            if isIncludedModel2View {
                stackView.removeArrangedSubview(dateBuyView)
                stackView.removeArrangedSubview(telephoneNumberView)
                stackView.removeArrangedSubview(fullnameView)
                stackView.addArrangedSubview(model2View)
                stackView.addArrangedSubview(dateBuyView)
                stackView.addArrangedSubview(telephoneNumberView)
                stackView.addArrangedSubview(fullnameView)
                
            }else {
                stackView.removeArrangedSubview(model2View)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstnameTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        lastnameTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        phoneNumberTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        modelTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        serialNumberTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        dateBuyTxf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        phoneNumberTxf.addTarget(self, action: #selector(textFieldDidEndEditing(_:)), for: .editingDidEndOnExit)
        
        modelTxf.delegate = self
        serialNumberTxf.delegate = self
        dateBuyTxf.delegate = self
        isIncludedModel2View = false
        
        sendBtn.setBackgroundColor(color: ColorHelper.red, forState: .normal)
        sendBtn.setBackgroundColor(color: ColorHelper.lightRed, forState: .disabled)
        sendBtn.isEnabled = (model2Txf.text == "" || firstnameTxf.text == "" || lastnameTxf.text == "" || phoneNumberTxf.text == "" || modelTxf.text == "" || serialNumberTxf.text == "" || dateBuyTxf.text == "" ) ? false:true
        
//        isIncludedModel2View = true
        
        
        
//        dateBuyTxf.text = Helper.formatDateToString(date: Date(), format: "dd/MM/yyyy")
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case usernameTxf:
            break
        case phoneNumberTxf:
            break
        case modelTxf:
            presentToScanView(type: "m")
        case serialNumberTxf:
//            presentToScanView(type: "sn")
            break
        case dateBuyTxf:
            break
        default:
            break
        }
        
//        sendBtn.isEnabled = (usernameTxf.text == "" || phoneNumberTxf.text == "" || modelTxf.text == "" || serialNumberTxf.text == "" || dateBuyTxf.text == "" ) ? false:true
        sendBtn.isEnabled = (model2Txf.text == "" || firstnameTxf.text == "" || lastnameTxf.text == "" || phoneNumberTxf.text == "" || modelTxf.text == "" || serialNumberTxf.text == "" || dateBuyTxf.text == "" ) ? false:true
        
    }
    
    @objc func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case phoneNumberTxf:
            if let text = phoneNumberTxf.text {
                AppService.validTelephoneNumber(telephoneNumber: text) { (isSuccess, error) in
                    if (isSuccess) {
                        print("login thanh cong")
                    }else {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Lỗi", message: error?.localizedDescription ?? "validate fail", preferredStyle: .alert)
                            let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: nil)
                            alert.addAction(actionOk)
                            self.present(alert, animated: true, completion: nil)
                            self.phoneNumberTxf.text = ""
                        }
                    }
                }
            }
            
        case modelTxf:
            break
        case serialNumberTxf:
            if let serialNumber = serialNumberTxf.text {
                AppService.validSerialNumber(serialNumber: serialNumber) { (isSuccess,listProduct, error) in
                    if (isSuccess) {
                        self.list_Product = listProduct
                    }else {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Lỗi", message: error?.localizedDescription ?? "validate fail", preferredStyle: .alert)
                            let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: nil)
                            alert.addAction(actionOk)
                            self.present(alert, animated: true, completion: nil)
                            self.phoneNumberTxf.text = ""
                        }
                    }
                }
            }
        case dateBuyTxf:
            break
        default:
            break
        }
        
//        sendBtn.isEnabled = (usernameTxf.text == "" || phoneNumberTxf.text == "" || modelTxf.text == "" || serialNumberTxf.text == "" || dateBuyTxf.text == "" ) ? false:true
        sendBtn.isEnabled = (model2Txf.text == "" || firstnameTxf.text == "" || lastnameTxf.text == "" || phoneNumberTxf.text == "" || modelTxf.text == "" || serialNumberTxf.text == "" || dateBuyTxf.text == "" ) ? false:true
        
        
    }
    
    
    
    func submitDataToServer() {
        if let model1 = modelTxf.text, 
            let serial = serialNumberTxf.text,
            let model2 = model2Txf.text, 
            let customerFirstName = firstnameTxf.text, 
            let customerLastName = lastnameTxf.text, 
            let telephone = phoneNumberTxf.text {
            AppService.customerRegistration(model1: model1, serial: serial, model2: model2, customerFirstName: customerFirstName, customerLastName: customerLastName, telephone: telephone) { isSuccess, error in
                if (isSuccess) {
                    let alert = UIAlertController(title: "Thông báo", message: error?.localizedDescription ?? "đăng kí thành công", preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: { (alertAction) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    alert.addAction(actionOk)
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }else {
                    print(error?.localizedDescription ?? "Registration fail")
                    
                    let alert = UIAlertController(title: "Lỗi", message: error?.localizedDescription ?? "Registration fail", preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: nil)
                    alert.addAction(actionOk)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
            
        
        
    }
    
    @IBAction func onSend(_ sender: UIButton) {
        submitDataToServer()
    }
    
    func presentToScanView(type: String) {
        let scannerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "scannerViewController") as! ScannerViewController
//        scannerVC.hero.isEnabled = true
//        scannerVC.type = type
//        scannerVC.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        self.present(scannerVC, animated: true, completion: nil)
    }
    
}

extension RegisterProductViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("myTargetFunction")
        switch textField {
        case modelTxf:
            self.view.endEditing(true)
            textField.resignFirstResponder()

            var listModel = [String]()
            
            for item in list_Product {
                if !item.model.isEmpty {
                    listModel.append(item.model)
                }
            }
            
            ActionSheetStringPicker.show(withTitle: "Chọn model 1", rows: listModel, initialSelection: 0, doneBlock: { (picker, indexes, values) in
                self.modelTxf.text = values as? String 
                for item in self.list_Product {
                    if item.model == values as? String ,
                        item.serviceItemGroup == "AC", 
                        !item.model2.isEmpty{
                        
                        self.isIncludedModel2View = true
                        self.model2Txf.text = item.model2
                        return
                    }else {
                        self.isIncludedModel2View = false
                    }
                }
                return
            }, cancel: { (actionMultipleStringCancelBlock) in
                
            }, origin: modelTxf)
            
        case serialNumberTxf:
//            presentToScanView(type: "sn")
            break
        case dateBuyTxf:
            let calendarVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "calendarVC") as! CalendarViewController
//            calendarVC.hero.isEnabled = true
//            calendarVC.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            
            // Callback from CalendarViewController
            calendarVC.selectedDate = { (date) in
                self.dateBuyTxf.text = Helper.formatDateToString(date: date, format: "dd/MM/yyyy")
            }
            
            self.present(calendarVC, animated: true, completion: nil)
        default:
            break
        }
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
}
