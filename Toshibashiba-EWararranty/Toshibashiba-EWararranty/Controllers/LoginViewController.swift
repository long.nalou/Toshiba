//
//  LoginViewController.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import UIKit

class LoginViewController: ViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        signInBtn.setBackgroundColor(color: ColorHelper.red, forState: .normal)
        signInBtn.setBackgroundColor(color: ColorHelper.lightRed, forState: .disabled)
        signInBtn.isEnabled = (usernameTextField.text == "" || passwordTextField.text == "" ) ? false:true
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        signInBtn.isEnabled = (usernameTextField.text == "" || passwordTextField.text == "" ) ? false:true
    }
    @IBAction func onSignIn(_ sender: UIButton) {
        if let username = usernameTextField.text, 
            let password = passwordTextField.text {
            AppService.loginUser(username: username, password: password) { [weak self](isSuccess, error) in
                if (isSuccess) {
                    print("login thanh cong")
                    ObjectManager.default.username = username
                    if let rootVC = self?.storyboard?.instantiateViewController(withIdentifier: "RootViewController") as? RootViewController {
                        self?.present(rootVC, animated: true, completion: nil)
                    }
                }else {
                    print(error?.localizedDescription ?? "login fail")
                    
                    let alert = UIAlertController(title: "Lỗi", message: error?.localizedDescription ?? "login fail", preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "Đồng ý", style: .cancel, handler: nil)
                    alert.addAction(actionOk)
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
}
