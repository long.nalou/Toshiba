//
//  ProductModel.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/9/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import Foundation

struct ProductModel  {
    var status: String 
    var model: String
    var model2: String
    var serviceItemGroup: String
    
    init(json: NSDictionary?) {
        self.status = json?.object(forKey:"status") as? String ?? ""
        self.model = json?.object(forKey:"model") as? String ?? ""
        self.model2 = json?.object(forKey:"model2")  as? String ?? ""
        self.serviceItemGroup = json?.object(forKey:"serviceItemGroup") as? String ?? ""
    }
    
}
