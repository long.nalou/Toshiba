//
//  ObjectManager.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import Foundation


class ObjectManager: NSObject {
    private override init() { }
    static let `default` = ObjectManager()
    var token = ""
    var username = ""
}
