//
//  ColorHelper.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import Foundation
import UIKit

open class ColorHelper:NSObject {
    
    @objc open static let red = Hex.colorFromHex("cc0000")
    @objc open static let lightRed = Hex.colorFromHex("FDAAAB")
    
}

private class Hex {
    fileprivate class func colorFromHex(_ colorCode: String) -> UIColor {
        let scanner = Scanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}
