//
//  HTTPConstants.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import Foundation

import Foundation

struct HTTPConstants {
    
    static let baseUrl                      =   "http://123.30.155.149:8081/"
    
    
    // API URL
    static let kRoutePostLogin              =   "api/user/token"
    static let kRoutePostChangePassword     =   "api/user/Subdealerchangepassword"
    static let kRoutePostValidTelephone     =   "api/warranty/CustomerValidTelephone?TelephoneNumber=%@"
    static let kRoutePostCustomerRegistration =   "api/warranty/CustomerRegistration"
    static let kRoutePostValidSerialNumber  =   "api/warranty/CustomerValidSerialNumber?SerialNumber=%@"
    
    
    
    //Header keys
    static let kHeadersKeyAuthorization     =   "Authorization"
    static let kHeadersKeyContentType       =   "Content-Type"
}
