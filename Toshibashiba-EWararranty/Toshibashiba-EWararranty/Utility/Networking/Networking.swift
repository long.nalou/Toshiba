//
//  Networking.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/8/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkRouter : URLRequestConvertible {
    case loginUser(username: String, password: String)
    case changePass(username: String, oldPassword: String, newPassWord: String)
    case validTelephoneNumber(telephoneNumber: String)
    case customerRegistration(model1: String, serial: String, model2: String, customerFirstName: String, customerLastName: String, telephone: String)
    case validSerialNumber(serialNumber: String)
    
    
    var path: String {
        switch self {
        case .loginUser:
            return HTTPConstants.kRoutePostLogin
        case .changePass:
            return HTTPConstants.kRoutePostChangePassword
        case let .validTelephoneNumber(telephoneNumber: telephoneNumber):
            return String(format: HTTPConstants.kRoutePostValidTelephone, telephoneNumber)
        case .customerRegistration:
            return HTTPConstants.kRoutePostCustomerRegistration
        case let .validSerialNumber(serialNumber: serialNumber):
            return String(format: HTTPConstants.kRoutePostValidSerialNumber, serialNumber)
            
        }
    }
    
    
    
    var body: Data {
        var bodyDict: [String: Any] = [:]
        
        switch self {
        case let .loginUser(username: username, password: password):
            bodyDict["username"] = username
            bodyDict["password"] = password
        case let .changePass(username: username, oldPassword: oldPassword, newPassWord: newPassWord):
            bodyDict["UserName"] = username
            bodyDict["Password"] = newPassWord
            bodyDict["OldPassword"] = oldPassword
        case .validTelephoneNumber, .validSerialNumber:
            break
            
        case let .customerRegistration(model1: model1, serial: serial, model2: model2, customerFirstName: customerFirstName, customerLastName: customerLastName, telephone: telephone):
            bodyDict["Model"] = model1
            bodyDict["Serial"] = serial
            bodyDict["Model2"] = model1
            bodyDict["Serial2"] = model2
            bodyDict["CustomerFirstName"] = customerFirstName
            bodyDict["CustomerLastName"] = customerLastName
            bodyDict["Telephone"] = telephone
            
        }
        
        let data = try! JSONSerialization.data(withJSONObject: bodyDict, options: .prettyPrinted)
        
        return data
    }
    
    
    
    
    var method: HTTPMethod {
        switch self {
        case .loginUser, .changePass, .validTelephoneNumber, .customerRegistration, .validSerialNumber:
            return .post
        }
    }
    
    
    
    
    
    
    var headers: HTTPHeaders {
        var headers: [String:String] = [:]
        
        switch self {
        case .loginUser, .changePass, .validTelephoneNumber, .customerRegistration, .validSerialNumber:
            headers[HTTPConstants.kHeadersKeyContentType] = "application/json"
            headers[HTTPConstants.kHeadersKeyAuthorization] = String(format: "Bearer %@", ObjectManager.default.token)

        }
        
        return headers
    }
    
    func asURLRequest() throws -> URLRequest {
        let urlString = HTTPConstants.baseUrl + path
        let url = try urlString.asURL()
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = method.rawValue
        if method.rawValue != "GET" {
            urlRequest.httpBody = body
        }
        
        urlRequest.allHTTPHeaderFields = headers
        
        return try urlRequest
    }
}
