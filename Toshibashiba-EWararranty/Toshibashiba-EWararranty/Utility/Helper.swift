//
//  Helper.swift
//  Toshibashiba-EWararranty
//
//  Created by DXC-Technology on 9/9/18.
//  Copyright © 2018 Long Nguyen. All rights reserved.
//

import Foundation

class Helper {
    class func formatDateToString(date: Date, format: String) -> String {
        let formater = DateFormatter()
        formater.dateFormat = format
        
        return formater.string(from: date)
    }
}
